# Importação de módulos de ML
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
# Demais módulos
import pandas as pd
import datetime
from tqdm import tqdm

# Definição de constantes
_NOW = lambda: datetime.datetime.now()
_TIME_START = _NOW()


def get_recommendations(read_path: str, write_path: str, num_recommendations: int=3, limit_products: int=None):
    """Função para executar um modelo de ML de Similaridade de Cossenos para criar combos
    personalizados com itens recomendados.

    Parâmetros:
        * `read_path`: str Caminho onde está o dado utilizado para treinar o modelo
        * `write_path`: str Caminho onde o dado resultado do modelo será escrito
        * `num_recommendations`: int Número de recomendações que devem ser consideradas para cada item. Valor default `3`.
        * `limit_products`: int Número de produtos que devem ser considerados no DataFrame final. Valor default `None` considera todos os registros.

    Descrição da técnica utilizada:
        A técnica de similaridade de cosseno é uma medida que calcula a semelhança entre dois vetores, sendo requentemente utilizada em problemas de recomendação, agrupamento, classificação de texto.
        Nesse contexto, cada item (produto, documento, etc.) é representado como um vetor em um espaço multidimensional, onde cada dimensão corresponde a uma característica relevante.
        É calculado o cosseno do ângulo entre os vetores para determinar a similaridade entre dois itens que os representam.
        Quanto mais próximos os vetores estiverem na direção e sentido, mais similaridade eles terão, com um valor de 1 indicando total similaridade e 0 indicando completa dissimilaridade.

    """
    time_start = _NOW()

    # Prepara os dados de treinamento mantendo apenas os IDs únicos dos produtos e suas descrições
    df_train = pd.read_csv(read_path).sort_values(
        'receita_bruta', ascending=False
    ).drop_duplicates(
        ['item_id', 'item_descricao']
    )[['item_id', 'item_descricao']]

    # Lista de produtos
    products = df_train['item_id'].tolist()

    # Lista das descrições dos produtos
    descriptions = df_train['item_descricao'].tolist()

    # Vetorização das descrições que serão utilizadas no modelo de Similaridade de Cossenos
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(descriptions)

    # Função para recomendação de produtos
    def recommend_products(product_index, num_recommendations=3):
        similarities = cosine_similarity(X, X[product_index])
        similar_indices = similarities.argsort(axis=0)[-num_recommendations - 1:-1][::-1]
        recommended_products = [
            (products[i[0]], similarities[i[0]][0], descriptions[i[0]])
                for i in similar_indices
        ]
        # Retorna os ids, similaridades e descrições dos produtos recomendados
        return recommended_products

    # Estrutura do DataFrame resultante
    result = {
        'item_id': [],
        'item_description': [],
        'item_recomendation_id': [],
        'item_recomendation_description': []
    }

    # Obtém as recomendações para todos os produtos/itens
    for product_index in tqdm(range(limit_products if limit_products else df_train.shape[0])):
        recommendations = recommend_products(product_index, num_recommendations)
        # print(f"Produtos recomendados para\n  `{products[product_index]} - {descriptions[product_index]}`:")
        # Iteração para preencher o dicionário que será utilizado para construir o DF com as informações dos itens recomendados
        for product, similarity, descricao in recommendations:
            # print(f"\t > {product} - {descricao}")
            result['item_id'].append(products[product_index])
            result['item_description'].append(descriptions[product_index])
            result['item_recomendation_id'].append(product)
            result['item_recomendation_description'].append(descricao)
        # print()

    # Constrói o DataFrame e escreve os dados
    pd.DataFrame(result).to_csv(write_path, index=False)

    print(f"Dados escritos com sucesso em: {write_path}")
    print(f"Tempo de execução total: {_NOW() - time_start}")
