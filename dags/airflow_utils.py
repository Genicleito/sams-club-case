# Lib com utilitários para execução das DAGs que estão no Apache Airflow

from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator
from airflow.providers.google.cloud.operators.bigquery import BigQueryExecuteQueryOperator # Operador que executa uma query no BigQuery
from airflow.operators.python_operator import PythonOperator # Operador Python
from airflow.operators.dummy_operator  import DummyOperator # Operador que "não faz nada"
from google.cloud import storage # Utilizado para caso seja necessário ler informações do Storage
import datetime # Módulo Python para obter a data atual

def get_bigquery_execute_query_operator(
        dag: DAG,
        task_id: str,
        sql_file_path_or_sql_query: str,
        destination_dataset_table: str,
        gcp_conn_id: str,
        write_disposition: str = 'WRITE_TRUNCATE',
        use_legacy_sql: bool = False,
        allow_large_results: bool = True,
        labels: dict = None
    ) -> BigQueryExecuteQueryOperator:
    """Função responsável por retornar uma task que executa uma query no BigQuery e escreve o resultado em uma tabela.
    """
    # Verifica se foi passado um caminho para um arquivo `.sql` ou uma string com a query SQL
    if sql_file_path_or_sql_query.endswith('.sql'):
        with open(sql_file_path_or_sql_query) as f:
            sql_query = f.read()
    else:
        sql_query = sql_file_path_or_sql_query

    # Retorna o operador do Airflow com os parâmetros informados
    return BigQueryExecuteQueryOperator(
        dag=dag,
        task_id=task_id,
        sql=sql_query,
        destination_dataset_table=destination_dataset_table,
        write_disposition=write_disposition,
        use_legacy_sql=use_legacy_sql,
        gcp_conn_id=gcp_conn_id,
        allow_large_results=allow_large_results,
        labels=labels
    )

def get_task_gcs_to_bigquery(
        dag: DAG,
        task_id: str,
        bucket: str,
        source_objects: list,
        destination_project_dataset_table: str,
        write_disposition: str = "WRITE_TRUNCATE",
        autodetect=True,
    )  -> GCSToBigQueryOperator::
    """Função responsável por retornar uma task que envia dados (PARQUET ou CSV) do GCS para o BQ
    """
    return GCSToBigQueryOperator(
        dag=dag,
        task_id=task_id,
        bucket=bucket,
        source_objects=source_objects,
        destination_project_dataset_table=destination_project_dataset_table,
        write_disposition=write_disposition,
        autodetect=autodetect,
    )

def dummy_task(dag, task_id, trigger_rule='all_success'):
    """Função que retorna uma tarefa dummy com os parâmetros passados.

    Parameters:
        * `dag`: DAG onde a task será inserida
        * `task_id`: ID da task
        * `trigger_rule`: `trigger_rule` da task. Default: `all_success`

    Return:
        DummyOperator
    """
    return DummyOperator(dag=dag, task_id=task_id, trigger_rule=trigger_rule)
