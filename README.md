### Informações Relevantes

* Estes dados estão em um repositório público no GitLab: <https://gitlab.com/Genicleito/sams-club-case>

Este repositório possui todos os dados e resultados obtidos ao construir o case técnico.

Os códigos estão minimamente modularizados para facilitar o fluxo de execução.

#### A pasta `assets`
Na pasta `assets` existem prints do Dashboard da Q1, além de um PDF com o layout do Dashboard. Também foi incluída uma imagem `bpmn_dag.png` com a modelagem de processos das execuções desse pequeno pipeline de dados.
Há, pasta _assets_, também, um print com o trecho do output da Q2. Além disso, os dados da Q2 estão no caminho `datalake/trusted/dados_comerciais.csv`.

#### A pasta `datalake`
Nesta pasta existem os dados pertinentes gerados pelos scripts, devidamente segmentados por zonas `raw` (dado original fornecido) e `trusted` (dados gerados pelo pipeline).

Na zona `trusted` existem, além do `dados_comerciais.csv`, também o arquivo `parte2_ds.csv` com o output após rodar uma técnica de ML para obter combos de itens conforme entendimento da Q2. Alémn do print, também está sendo disponibilizado o arquivo completo gerado. Este arquivo, inclusive, é utilizado no pipeline da DAG do airflow simulando o seu envio para o BigQuery e posterior análise em ferramentas de BI (DataStudio, Metabase, etc).

#### A pasta `dags`
Nesta pasta estão todos os scripts e libs desenvolvidas para executar as etapas do teste prático, além do código da própria DAG, construída com Airflow numa estrutura muito próxima à real para execução desse pipeline de dados e envio para o BQ.
