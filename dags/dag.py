from airflow import DAG # Módulo principal para construção da DAG
from airflow.utils.dates import days_ago
import datetime
import os
import sys
import inspect
import logging

# Obtém o caminho corrente onde está o arquivo da DAG
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# Obtém o caminho anterior ao que está o arquivo da DAG
parent_dir = os.path.dirname(current_dir)
# Adiciona os caminhos ao PATH do sistema para permitir a importação das libs criadas
sys.path.insert(0, parent_dir) 
sys.path.insert(0, current_dir)

# Exibe informações no log da DAG no Airflow
logging.info(f'CURRENT_DIR: \t{current_dir}')

# Importa a função que cria tasks para rodar query SQL no BigQuery
from airflow_utils import (
    get_task_gcs_to_bigquery, dummy_task, get_bigquery_execute_query_operator
)

# Importa definições e regras de negócio do projeto do arquivo `./project_config.py`
from project_config import (
    Q1_QUERY, PROJECT_ID, OUTPUT_BUCKET, OUTPUT_DATASET_BIGQUERY, SAMS_CLUB_CASE_SQL_TABLE,
    DADOS_COMERCIAIS_PATH, PARTE2_DS_PATH, PARTE2_DS_BQ_TABLE, CASE_PRATICO_RAW_DATA_PATH
)

# Importa funções utilitárias para construir, executar e validar modelos de Machine Learning
from ml_utils import (
    get_recommendations
)

doc_md_dag = """DAG construída para executar as tasks necessárias para o Case Técnico Sam's Club
"""

default_args = {
    'owner': 'genicleito',
    'depends_on_past': False,
    'start_date': days_ago(3),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 3, # Tenta executar as tasks por 3 vezes até setar o status `failed``
    'retry_delay': timedelta(seconds=30), # Tempo de 30 segundos entre cada retry
    'on_failure_callback': lambda: print(f"Tarefa falhou! (Função executada sempre que a task falha)")
}

# Cria a DAG
dag = DAG(
    'data_pipeline',
    catchup=False,
    default_args=default_args,
    description='DAG responsável por executar tasks de data engineer e analytics engineer.',
    schedule_interval='0 7 * * 4', # CRON scheduler para toda quinta feira (4) às 7 horas (UTC do Airflow)
    doc_md=doc_md_dag,
)

# Tasks dummy apenas para organizar o início e fim do pipeline
start_task = dummy_task(dag=dag, task_id='dummy_start_pipeline')
end_task = dummy_task(dag=dag, task_id='dummy_end_pipeline')

# Task para executar a query SQL no BigQuery e criar uma tabela com o resultado do output (Parte 1 - Analytics)
gerar_base_q1_task = get_bigquery_execute_query_operator(
    dag=dag,
    task_id='gerar_base_q1',
    sql_file_path_or_sql_query=Q1_QUERY,
    # O parâmetro `destination_dataset_table` permite salvar o output em uma tabela do BQ
    destination_dataset_table=f'{PROJECT_ID}.{OUTPUT_DATASET_BIGQUERY}.{SAMS_CLUB_CASE_SQL_TABLE}',
    write_disposition='WRITE_TRUNCATE',
    use_legacy_sql=False,
    gcp_conn_id=GCP_CONN_ID, # Connection do airflow com o keyfile JSON ou com o keyfile_path para o arquivo .json de autenticação do GCP
    allow_large_results=True, # Permite grandes volumes de dados como output da query
    execution_timeout=datetime.timedelta(minutes=30), # Tempo máximo de execução de 30 minutos até lançar um erro
)

# Task para executar o código Python com solução aplicando técnica de Machine Learning para encontrar combos
get_recommendations_task = PythonOperator(
    dag=dag,
    task_id='parte_2_ml_run',
    python_callable=get_recommendations,
    op_kwargs={
        'read_path': CASE_PRATICO_RAW_DATA_PATH,
        'write_path': PARTE2_DS_PATH,
    },
    execution_timeout=datetime.timedelta(hours=1)
)

# Task para executar o código da Parte 2 - Data Sciense (DS)
parte2_gcs_to_bq_task = get_task_gcs_to_bigquery(
    dag=dag,
    task_id="parte2_gcs_to_bq",
    bucket=OUTPUT_BUCKET,
    source_objects=[f"{PARTE2_DS_PATH.split(OUTPUT_BUCKET)[1][1:]}"], # .csv
    destination_project_dataset_table=f'{PROJECT_ID}.{OUTPUT_DATASET_BIGQUERY}.{PARTE2_DS_BQ_TABLE}',
    write_disposition="WRITE_TRUNCATE",
    autodetect=True,
)


# Criação das dependências das tasks
start_task >> gerar_base_q1_task >> get_recommendations_task >> parte2_gcs_to_bq_task >> end_task
