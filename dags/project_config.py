#################### Constantes do Cloud Storage ####################
OUTPUT_BUCKET = "datalake"

#################### Constantes de Paths ####################
Q1_QUERY = f"gs://{OUTPUT_BUCKET}/sandbox/q1.sql" # Pode ser um arquivo `.sql` ou uma query (ex: "SELECT * FROM TABLE_A")
TRUSTED_PATH = f"gs://{OUTPUT_BUCKET}/trusted"
RAW_PATH = f"gs://{OUTPUT_BUCKET}/trusted"

#################### Paths de Datasets ####################
DADOS_COMERCIAIS_PATH = f"{TRUSTED_PATH}/dados_comerciais.csv"
PARTE2_DS_PATH = f"{TRUSTED_PATH}/parte2_ds.csv"
CASE_PRATICO_RAW_DATA_PATH = f"{RAW_PATH}/CASE_PRATICO_SAMS_CLUB.zip"

#################### Constantes do BigQuery ####################
PROJECT_ID = "sams" # Valor meramente de exemplo
OUTPUT_DATASET_BIGQUERY = "raw" # Valor meramente de exemplo
SAMS_CLUB_CASE_SQL_TABLE = "sams_club_case" # Valor meramente de exemplo
PARTE2_DS_BQ_TABLE = "parte2_ds"
